package classes
{
	/**
	 AGORA - an interactive and web-based argument mapping tool that stimulates reasoning, 
	 reflection, critique, deliberation, and creativity in individual argument construction 
	 and in collaborative or adversarial settings. 
	 Copyright (C) 2011 Georgia Institute of Technology
	 
	 This program is free software: you can redistribute it and/or modify
	 it under the terms of the GNU Affero General Public License as
	 published by the Free Software Foundation, either version 3 of the
	 License, or (at your option) any later version.
	 
	 This program is distributed in the hope that it will be useful,
	 but WITHOUT ANY WARRANTY; without even the implied warranty of
	 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 GNU Affero General Public License for more details.
	 
	 You should have received a copy of the GNU Affero General Public License
	 along with this program.  If not, see <http://www.gnu.org/licenses/>.
	 
	 */
	import Model.StatementModel;
	
	import ValueObjects.AGORAParameters;
	
	import classes.Configure;
	import classes.Language;
	
	import components.ArgSelector;
	import components.GridPanel;
	import components.Option;
	
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.events.Event;
	import flash.events.FocusEvent;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	import flash.ui.Keyboard;
	
	import logic.ConditionalSyllogism;
	import logic.ParentArg;
	
	import mx.binding.utils.BindingUtils;
	import mx.containers.Canvas;
	import mx.controls.Alert;
	import mx.controls.Label;
	import mx.controls.Menu;
	import mx.controls.Text;
	import mx.controls.TextInput;
	import mx.core.DragSource;
	import mx.core.UIComponent;
	import mx.events.DragEvent;
	import mx.events.EventListenerRequest;
	import mx.events.FlexEvent;
	import mx.events.MenuEvent;
	import mx.managers.DragManager;
	import mx.skins.Border;
	
	import org.osmf.events.GatewayChangeEvent;
	
	import spark.components.Button;
	import spark.components.Group;
	import spark.components.HGroup;
	import spark.components.Panel;
	import spark.components.TextArea;
	import spark.components.VGroup;
	import spark.effects.Resize;
	import spark.layouts.HorizontalAlign;
	import spark.layouts.HorizontalLayout;
	import spark.layouts.VerticalLayout;
	import spark.skins.spark.PanelSkin;
	
	public class ArgumentPanel extends GridPanel
	{
		//model class
		public var statementModel:StatementModel;
		
		//Input boxes
		//The text box in which the user enters the argument
		public var input1:DynamicTextArea;
		//Right now the requirement is for only two,
		//but this is extensible
		public var inputs:Vector.<DynamicTextArea>;
		//dragging handle	
		
		//Text display elements
		//A statment exists in two states: editable, and non-editable. When
		//the user clicks the done button, it goes to the non-editable state.
		//The input textboxes are hidden and the below Text control is shown.
		public var displayTxt:Text;
		//label for displaying 'It is not the case that' for netaged
		//statements
		public var negatedLbl:Label;
		//Displays the type of this statment
		public var stmtTypeLbl:Label;
		//Displays the user id
		public var userIdLbl:Label;
		
		//control elements
		public var topArea:UIComponent;
		//doneButton
		public var doneBtn:AButton;
		//add button
		public var addBtn:AButton;
		//delete button
		public var deleteBtn:AButton;
		
		//appearance
		//skin of the panel
		public var panelSkin:PanelSkin;
		
		//State Variables
		//state=0 -> universal statement and state=1 -> particular statement
		public var state:int;	
		//Takes either INFERENCE or ARGUMENT_PANEL
		public var panelType:int;
		//Specifies whether the statement is negative or positive
		private var _statementNegated:Boolean;		
		//Before a user enters text into the statement, it is false
		public var userEntered:Boolean;
		//claim added through start with claim
		public var firstClaim:Boolean;
		//multiple textboxes
		private var _multiStatement:Boolean;
		//type of multistatement
		private var _implies:Boolean;
		//whether the node has permanent ID generated by the server
		public var hasID:Boolean;
		//for input1
		public var input1NTHasID:Boolean;
		//for inputs
		public var inputsNTHasID:Vector.<Boolean>;
		
		//constants
		//Type of Panel: this could be found by just using is operator
		public static const ARGUMENT_PANEL:int = 0;
		//Type of Panel
		public static const INFERENCE:int = 1;
		//connecting string constants required for multistatements
		public static var IF_THEN:String = "If-then";
		public static var IMPLIES:String = "Implies";
		//Event
		public static var ARGUMENT_CONSTRUCTED:String = "Argument Constructed";
		
		//References to other objects
		//A reference to the current map diplayed to the user
		public static var parentMap:AgoraMap;
		//List of enablers which makes other statements support this statement
		public var rules:Vector.<Inference>;
		
		//Containers
		//The logical container that holds the text elements of the statement
		//that is, input boxes and displayTxt
		public var group:Group;
		//multistatement group
		public var msVGroup:VGroup;
		//The enabler which makes this statements support a claim
		public var inference:Inference;
		//contains the add and the delete button
		public var bottomHG:HGroup;
		//the logical container that contains everything above the group container
		public var topHG:HGroup;
		//Within the topHG. It holds the author information and the type of statement
		public var stmtInfoVG:VGroup;
		//Container that holds the done button
		public var doneHG:HGroup;
		//contains the doneHG and bottomHG
		public var btnG:Group;
		
		//Menu data
		//XML string holding the menu data for the add button
		public var addMenuData:XML;
		//XML string holding the menu data for the menu that pops up when user hits the done button
		public var constructArgData:XML;
	
		//other instance variables
		public var connectingStr:String;
		public var _initXML:XML = null;
		public var _properties:XML = null;
		public var ID:int;
		public var TID:int;
		//IDs for nodetext
		public var input1NTTID:int;
		public var input1NTID:int;
		public var inputsNTTID:Vector.<int>;
		public var inputsNTID:Vector.<int>;
		
		public function ArgumentPanel()
		{
			super();
			firstClaim = false;
			addMenuData = <root><menuitem label="add an argument for this statement" type="TopLevel" /></root>;
			constructArgData = <root><menuitem label="add another reason" type="TopLevel"/><menuitem label="construct argument" type="TopLevel"/></root>;
			
			userEntered = false;
			panelType = ArgumentPanel.ARGUMENT_PANEL;			
			
			inputs = new Vector.<DynamicTextArea>;
			
			//will be set by the object that creates this
			inference = null;
			width = 180;
			minHeight = 100;	
			
			//Event handlers
			addEventListener(FlexEvent.CREATION_COMPLETE, onCreationComplete);
		}
		
		override public function setX(value:int):void{
			y = value * AGORAParameters.getInstance().gridWidth;
		}
		
		override public function setY(value:int):void{
			x = value * AGORAParameters.getInstance().gridWidth;
		}
		
		protected function onCreationComplete(event:FlexEvent):void{
			panelSkin = this.skin as PanelSkin;
			panelSkin.topGroup.includeInLayout = false;
			panelSkin.topGroup.visible = false;
		}
		
		public function makeEditable():void
		{
			
		}
		
		
		public function get stmt():String
		{
			
			var statement:String = "";
			if(multiStatement)
			{
				if(implies)
				{
					if(connectingStr == "If-then")
						statement = Language.lookup("ArgIf") + inputs[1].text + "," 
										+ Language.lookup("ArgThen")+ inputs[0].text;
					else
						statement = inputs[1].text + Language.lookup("ArgImplies") + inputs[0].text;					
				}
				else
				{
					for(var i:int=0; i<inputs.length - 1; i++)
					{
						statement = statement + inputs[i].text + Language.lookup("ArgAnd");
					}
					statement = statement + inputs[i].text;
				}
			}
			else
			{
				statement = input1.text;
			}
			if(statementNegated == true)
			{
				return (Language.lookup("ArgNotCase") + statement);
			}
			else
			{
				return statement;
			}
		}
		
		protected function lblClicked(event:MouseEvent):void
		{
			makeEditable();
		}
		
		public function keyEntered(event: KeyboardEvent):void
		{
			if(event.keyCode == Keyboard.ENTER)	
			{
				//statementEntered();	
			}
		}
		
		public function beginDrag( mouseEvent: MouseEvent ):void
		{
			try{
				var	dinitiator:UIComponent = UIComponent(mouseEvent.currentTarget);
				var dPInitiator:ArgumentPanel = this;
				var ds:DragSource = new DragSource();
				var tmpx:int = int(dPInitiator.mouseX);
				var tmpy:int = int(dPInitiator.mouseY);
				ds.addData(tmpx,"x");
				ds.addData(tmpy,"y");
				ds.addData(dPInitiator.gridX,"gx");
				ds.addData(dPInitiator.gridY,"gy");
				DragManager.doDrag(dPInitiator,ds,mouseEvent,null);
			}
			catch(error:Error)
			{
				Alert.show(error.toString());
			}
		}
		
		public function removeEventListeners():void
		{
		
		}
		
		protected function optionClicked(event:MouseEvent):void
		{
		}
		
		protected function hideOption(event:KeyboardEvent):void
		{
		}
		
		
		public function showMenu():void
		{
			var menu:Menu = Menu.createMenu(null,constructArgData,false);
			menu.labelField = "@label";
			//menu.addEventListener(MenuEvent.ITEM_CLICK, constructArgument);
			var globalPosition:Point = localToGlobal(new Point(0,this.height));
			menu.show(globalPosition.x,globalPosition.y);	
		}
		
		
		public function doneHandler(d:MouseEvent):void
		{	
		}
		
	
		//create children must be overriden to create dynamically allocated children
		override protected function createChildren():void
		{
			//Elements are constructed, initialized with properties, and attached to display list		
			//create the children of MX Panel
			super.createChildren();		
			var uLayout:VerticalLayout = new VerticalLayout;
			uLayout.paddingBottom = 10;
			uLayout.paddingLeft = 10;
			uLayout.paddingRight = 10;
			uLayout.paddingTop = 10;
			this.layout = uLayout;
			
			
			userIdLbl = new Label;
			
			stmtTypeLbl = new Label;
			// default setting    	
			if(this is Inference)
			{
				stmtTypeLbl.text = "";
				state = 0;
			}
			else
			{
				stmtTypeLbl.text = Language.lookup("Particular");
				//stmtTypeLbl.text = "Particular";
				state = 1;
			}
			stmtTypeLbl.toolTip = Language.lookup("ParticularUniversalClarification");
			//stmtTypeLbl.toolTip = "Please change it before commiting";

			bottomHG = new HGroup();
			doneHG = new HGroup;
			doneBtn = new AButton;
			doneBtn.label = Language.lookup("Done");
			doneHG.addElement(doneBtn);
			
			input1 = new DynamicTextArea();
			input1.panelReference = this;
			input1.toolTip = "Otherwise, if you wish to start with Argument Scheme, click on the Add arg button below (do NOT press enter too)"; //TODO: Translate
			BindingUtils.bindProperty(input1, "text", statementModel, ["statement","text"]);
			//TODO: Translate
			displayTxt = new Text;
			BindingUtils.bindProperty(displayTxt, "text", input1, ["text"]);
			this.displayTxt.addEventListener(MouseEvent.CLICK, lblClicked);
			//Create a UIComponent for clicking and dragging
			topArea = new UIComponent;
			
			topHG = new HGroup();
			addElement(topHG);
			
			//Draw on topArea UIComponent a rectangle
			//to be used for clicking and dragging
			
			topArea.addEventListener(MouseEvent.MOUSE_DOWN,beginDrag);
			topArea.width = 40;
			topHG.addElement(topArea);
			//add a vertical subgroup
			stmtInfoVG = new VGroup;
			stmtInfoVG.gap = 0;
			topHG.addElement(stmtInfoVG);
			
			
			stmtInfoVG.addElement(stmtTypeLbl);
			stmtInfoVG.addElement(userIdLbl);
			
			userIdLbl.text = "AU: " + UserData.userNameStr;
			var userInfoStr:String = "User Name: " + UserData.userNameStr + "\n" + "User ID: " + UserData.uid;
			userIdLbl.toolTip = userInfoStr;
			
			negatedLbl = new Label;
			negatedLbl.text = Language.lookup("ArgNotCase");
			negatedLbl.visible = false;
			addElement(negatedLbl);
			
			
			group = new Group;
			addElement(group);
			group.addElement(input1);
			group.addElement(displayTxt);
			//displayTxt.addEventListener(FlexEvent.CREATION_COMPLETE,setGuidingText);
			input1.visible=false;
			
			btnG = new Group;
			addElement(btnG);
			btnG.addElement(bottomHG);
			doneHG = new HGroup;
			doneHG.addElement(doneBtn);
			btnG.addElement(doneHG);
			btnG.addElement(bottomHG);
			addBtn = new AButton;
			addBtn.label = Language.lookup("Add")+"...";
			
			bottomHG.addElement(addBtn);
			deleteBtn = new AButton;
			deleteBtn.label = "delete...";
			//deleteBtn.addEventListener(MouseEvent.CLICK,deleteThis);

			bottomHG.addElement(deleteBtn);
			//addBtn.addEventListener(MouseEvent.CLICK,addHandler);
			bottomHG.visible = false;
			
			//presently, the requirement is only for two boxes
			msVGroup = new VGroup;
			//group.addElement(msVGroup);
			var dta:DynamicTextArea = new DynamicTextArea;
			dta.panelReference = this;
			inputs.push(dta);
			dta = new DynamicTextArea;
			dta.panelReference = this;
			inputs.push(dta);
			for(var i:int=0; i < inputs.length; i++)
			{
				msVGroup.addElement(inputs[i]);
			}
			
			invalidateProperties();
		}

		override protected function commitProperties():void
		{
			super.commitProperties();	
		}
		
		override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void
		{
			super.updateDisplayList(unscaledWidth, unscaledHeight);
			topArea.graphics.beginFill(0xdddddd,1.0);
			topArea.graphics.drawRect(0,0,40,stmtInfoVG.height);
			userIdLbl.setActualSize(this.width - stmtInfoVG.x - 10, userIdLbl.height);		
		}

	}
	
}
